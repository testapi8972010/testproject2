*** Settings ***
Library  RequestsLibrary
Library  Collections


Test setup  create session  mysession  http://127.0.0.1:8000
*** Variables ***
${Relurl}  /
*** Test Cases ***
Get Request Test 
   ${responsepath}=  get on session  mysession  ${Relurl}  expected_status=200 
   log to console  ${responsepath.content}

   ${data}=  create dictionary  item_Id=003  q=bag
   ${responseget}=  get on session  mysession  /items/003  data=${data}  expected_status=200 
   
   log to console  ${responseget.content}
   ${contenttypevalue}=  get from dictionary  ${responseget.headers}  content-Type
   should be equal  ${contenttypevalue}  application/json 
   should be equal as strings  ${responseget.reason}  OK

Put Request test on item_Id
   ${data1}=  create dictionary  item_Id=046 name=bag  price=20.0   is_offer=true
   ${header}=  create dictionary  content-Type=application/json  
   ${responseput}=  put on session  mysession  /items/046  data=${data1}  headers=${header}  expected_status=200
   
   log to console  ${responseput.content} 

